#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTime>
#include <QDir>
#include <QLabel>
#include <QProcess>
#include <usersmodel.h>
#include "edituser.h"

namespace Ui {
class MW;
}

class MW : public QMainWindow
{
    Q_OBJECT

public:
    explicit MW(QWidget *parent = nullptr);
    ~MW();
    QDir HomeDir();
    QDir HomePath(QString path);
    void save(QString filename);
    void load(QString filename);

private slots:
    void terminalRead();
    void cmdprocessed(QString cmd);
    void on_addButton_clicked();
    void on_editButton_clicked();
    void on_removeButton_clicked();
    void on_checkButton_clicked();

    void on_blockButton_clicked();

private:
    Ui::MW *ui;
    QTcpSocket terminal;
    UsersModel users;
    QList<QString> listMAC;
    QLabel status;
    int state;

    void cleanLine(QByteArray *data);
    void fillMAC(QString data);
    void compareMAC();
    bool existMAC(QString MAC);
    void blockWorkStation(QString ip);

};

#endif // MW_H
