#include "edituser.h"
#include "ui_edituser.h"

EditUser::EditUser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditUser)
{
    ui->setupUi(this);
}

EditUser::~EditUser()
{
    delete ui;
}

QString EditUser::getName()
{
    return ui->nameEdit->text();
}

QString EditUser::getMAC()
{
    return ui->MACEdit->text();
}

QString EditUser::getIP()
{
    return ui->ipEdit->text();
}

void EditUser::setName(QString name)
{
    ui->nameEdit->setText(name);
    ui->buttonOk->setText(tr("Save"));
}

void EditUser::setMAC(QString mac)
{
    ui->MACEdit->setText(mac);
}

void EditUser::setIP(QString ip)
{
    ui->ipEdit->setText(ip);
}

void EditUser::on_buttonOk_clicked()
{
    accept();

   // close();
}
