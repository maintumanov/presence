#ifndef USERSMODEL_H
#define USERSMODEL_H

#include <QAbstractItemModel>
#include <QFile>
#include <QDataStream>
#include <QDir>


class UsersModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    struct user {
        int state;
        QString name;
        QString wsIP;
        QString MAC;
    };

    QList<user> users;

    explicit UsersModel(QObject *parent = Q_NULLPTR);
    ~UsersModel();

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role) const;
    void saveToStream(QDataStream *stream);
    void loadFromStream(QDataStream *stream);
    void addItem(QString name, QString MAC, QString IP);
    void editItem(QModelIndex index, QString name, QString MAC, QString IP);
    void removeItem(const QModelIndex &index);
    void setState(int row, int state);

    void listClear();
signals:
    void restoreBegin();

public slots:
    //    void currentStateBackup();
    //    void restore(const QModelIndex &index);
    //    void remove(const QModelIndex &index);

private:


};

#endif // USERSMODEL_H
