#include "mw.h"
#include "ui_mw.h"

MW::MW(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MW)
{
    ui->setupUi(this);
    ui->tableUsers->setModel(&users);
    connect(&terminal, SIGNAL(readyRead()), this, SLOT(terminalRead()));
    // connect(&terminal, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(connectChange(QAbstractSocket::SocketState)));
    load("users.dat");
    statusBar()->addWidget(&status);
    status.setText(tr("Ready"));
}

MW::~MW()
{
    delete ui;
}

void MW::terminalRead()
{
    QByteArray in;
    in = terminal.readAll();
    cleanLine(&in);
    //ui->Log->appendPlainText(QString("hex: %1").arg(QString(in.toHex(' '))));
    QString line = QString(in);
    //if (line.indexOf("MAC") > -1) checkMAC(line);
    if (line.indexOf("MAC") > -1) fillMAC(line);
    //qDebug() << line;
    //ui->Log->appendPlainText(line);
    cmdprocessed(line);
}

//void MW::on_connectButton_clicked()
//{
//    terminal.connectToHost(ui->ipEdit->text(), 23);
//}

//void MW::connectChange(QAbstractSocket::SocketState state)
//{
//    //    ui->connectButton->setEnabled(!terminal.isOpen());
//    //    ui->disconnectButton->setEnabled(terminal.isOpen());
//    // qDebug() << "state " << state;
//}


void MW::cmdprocessed(QString cmd)
{
    if (cmd.indexOf("Login:", 0, Qt::CaseInsensitive) > -1) {
        status.setText(tr("Enter login"));
        terminal.write(QString("%1\r\n").arg(ui->loginEdit->text()).toLocal8Bit());
        state = 0;
    } else if (cmd.indexOf("Password:", 0, Qt::CaseInsensitive) > -1) {
        status.setText(tr("Enter password"));
        terminal.write(QString("%1\r\n").arg(ui->passwordEdit->text()).toLocal8Bit());
    } else if (cmd.indexOf("(config)>", 0, Qt::CaseInsensitive) > -1) {
        if (state == 0) {
            status.setText(tr("table request"));
            terminal.write(QString("show ip arp\r\n").toLocal8Bit());
        } else {
            status.setText(tr("disconnect"));
            terminal.disconnectFromHost();
        }
        state ++;
    }
    //else terminal.disconnectFromHost();

}

//void MW::checkMAC(QString mactable)
//{
//    QStringList list = mactable.split(" ",QString::SkipEmptyParts);
//    bool a = false;
//    for (int i = 0; i < list.count(); i ++) {
//        if (list[i].count() == 17) {
//            if (list[i] == ui->macEdit->text()) {
//                ui->Log->appendPlainText(QString("%1 - Присутствует").arg(QDateTime::currentDateTime().toString("hh:mm:ss")));
//                if (macstate < 1) ui->Log->appendPlainText(QString("%1 - Появился").arg(QDateTime::currentDateTime().toString("hh:mm:ss")));
//                macstate = 1;
//                a = true;
//            }
//            //ui->Log->appendPlainText(list[i]);
//        }
//    }
//    if (!a) {
//        ui->Log->appendPlainText(QString("%1 - отсутствует").arg(QDateTime::currentDateTime().toString("hh:mm:ss")));
//        if (macstate == 1) ui->Log->appendPlainText(QString("%1 - Исчез").arg(QDateTime::currentDateTime().toString("hh:mm:ss")));
//        macstate = 0;
//    }

//}


void MW::save(QString filename)
{
    QFile file(HomePath("presence").absoluteFilePath(filename));
    if (!file.open(QIODevice::WriteOnly)) return;
    QDataStream Out(&file);
    Out.setVersion(QDataStream::Qt_4_7);
    Out << QString(QLatin1String("PresenceUser"));

    users.saveToStream(&Out);
    file.close();
}

void MW::load(QString filename)
{
    QFile file(HomePath("presence").absoluteFilePath(filename));
    if (!file.open(QIODevice::ReadOnly)) {
        file.close();
        return;
    }
    QDataStream In(&file);
    In.setVersion(QDataStream::Qt_4_7);
    QString signature;
    In >> signature;
    if (signature != QLatin1String("PresenceUser")) return;
    users.loadFromStream(&In);

    file.close();
}

QDir MW::HomeDir()
{
    QDir HomeDir = QDir::home();
    const QString Folder = QLatin1String("signalnet");
    if (!HomeDir.exists(Folder)) HomeDir.mkpath(Folder);
    HomeDir.cd(Folder);
    return HomeDir;
}

QDir MW::HomePath(QString path)
{
    QDir hDir = HomeDir();
    if (!hDir.exists(path)) hDir.mkpath(path);
    hDir.cd(path);
    return hDir;
}


void MW::on_addButton_clicked()
{
    EditUser ed(this);
    if (ed.exec() == QDialog::Accepted) {
        users.addItem(ed.getName(), ed.getMAC(), ed.getIP());
        save("users.dat");
    }
}

void MW::on_editButton_clicked()
{
    QModelIndex index = ui->tableUsers->currentIndex();
    if (!index.isValid()) return;
    if (index.row() >= users.users.count()) return;

    EditUser ed(this);
    ed.setName(users.users.at(index.row()).name);
    ed.setMAC(users.users.at(index.row()).MAC);
    ed.setIP(users.users.at(index.row()).wsIP);
    if (ed.exec() == QDialog::Accepted) {
        users.editItem(index, ed.getName(), ed.getMAC(), ed.getIP());
        save("users.dat");
    }
}

void MW::on_removeButton_clicked()
{
    users.removeItem(ui->tableUsers->currentIndex());
    save("users.dat");
}

void MW::on_checkButton_clicked()
{
    terminal.connectToHost(ui->ipEdit->text(), 23);
    status.setText(tr("Connect"));
}

void MW::cleanLine(QByteArray *data)
{
    QByteArray r;
    r.append(0x1B);
    r.append(0x5B);
    r.append(0x4B);
    int i = data->indexOf(r);
    while (i > -1) {
        data->remove(i, 3);
        i = data->indexOf(r);
    }
}

void MW::fillMAC(QString data)
{
    QStringList list = data.split(" ",QString::SkipEmptyParts);
    listMAC.clear();
    for (int i = 0; i < list.count(); i ++) if (list[i].count() == 17) listMAC.append(list[i]);
    compareMAC();
}

void MW::compareMAC()
{
    status.setText(tr("user verification"));
    for (int u = 0; u < users.users.count(); u ++) {
        if (existMAC(users.users[u].MAC)) users.setState(u, 1);
        else users.setState(u, 0);
    }
}

bool MW::existMAC(QString MAC)
{
    for (int i = 0; i < listMAC.count(); i ++)
        if (MAC == listMAC.at(i)) return true;
    return false;
}

void MW::blockWorkStation(QString ip)
{
    QProcess console;
//    console.start("C:\\Windows\\System32\\wbem\\wmic.exe",
//                  QStringList() << QString("/node:%1").arg(ip)
//                  << QString("/USER:%1").arg(ui->adminloginEdit->text())
//                  << QString("/PASSWORD:%1").arg(ui->adminpasswordEdit->text())
//                  << "process call create \"cmd /c rundll32 user32.dll LockWorkStation\"");

//    console.start(QString("cmd /c wmic.exe /node:%1 /USER:%2 /PASSWORD:%3 process call create \"cmd /c rundll32 user32.dll LockWorkStation\"")
//                  .arg(ip).arg(ui->adminloginEdit->text()).arg(ui->adminpasswordEdit->text()));
    console.start(QString("cmd /c wmic.exe /node:%1 process call create \"cmd /c rundll32 user32.dll LockWorkStation\"")
                  .arg(ip).arg(ui->adminloginEdit->text()).arg(ui->adminpasswordEdit->text()));
   //     console.start("notepad.exe");
    console.waitForReadyRead();
    qDebug() << console.readAllStandardOutput();
}

void MW::on_blockButton_clicked()
{
    QModelIndex index = ui->tableUsers->currentIndex();
    if (!index.isValid()) return;
    if (index.row() >= users.users.count()) return;
    blockWorkStation(users.users.at(index.row()).wsIP);
}
