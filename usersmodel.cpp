#include "usersmodel.h"


UsersModel::UsersModel(QObject *parent) :
    QAbstractTableModel(parent)
{

}

UsersModel::~UsersModel()
{

}

int UsersModel::rowCount(const QModelIndex &) const
{
    return users.count();
}

int UsersModel::columnCount(const QModelIndex &) const
{
    return 4;
}

QVariant UsersModel::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
    case Qt::DisplayRole:
        switch (index.column()) {
        case 0: return users[index.row()].name;
        case 1: switch (users[index.row()].state) {
            case -1: return "-";
            case 1: return "+";
            default: return "?";
            }
        case 2: return users[index.row()].MAC;
        case 3: return users[index.row()].wsIP;
        }
        return QVariant();

    case Qt::TextColorRole:
        return QVariant();

    case Qt::DecorationRole:
        return QVariant();

    case Qt::ToolTipRole:
        return QVariant();

    case Qt::WhatsThisRole:
        return QVariant();

    case Qt::TextAlignmentRole:
        return QVariant();

    case Qt::BackgroundColorRole:
        return QVariant();
    }
    return QVariant();
}

Qt::ItemFlags UsersModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (!index.isValid()) return flags;
    return flags;
}

QVariant UsersModel::headerData(int section, Qt::Orientation , int role) const
{
    switch (role)  {
    case Qt::DisplayRole:
        switch (section) {
        case 0: return QString(tr("Name"));
        case 1: return QString(tr("State"));
        case 2: return QString(tr("MAC"));
        case 3: return QString(tr("Work station"));
        }
        break;

    case Qt::TextAlignmentRole:
        return Qt::AlignCenter;
    }
    return QVariant();
}

void UsersModel::saveToStream(QDataStream *stream)
{
    *stream << users.count();
    for (int i = 0; i < users.count(); i ++)
    {
        *stream << users[i].name;
        *stream << users[i].MAC;
        *stream << users[i].wsIP;
    }
}

void UsersModel::loadFromStream(QDataStream *stream)
{
    int count;
    *stream >> count;
    listClear();

    user item;
    if (count > 0) {
        beginInsertRows(QModelIndex(), 0, count - 1);
        for (int i = 0; i < count; i ++)
        {
            *stream >> item.name;
            *stream >> item.MAC;
            *stream >> item.wsIP;
            item.state = 0;
            users.append(item);
        }
        endInsertRows();
    }
}

void UsersModel::removeItem(const QModelIndex &index)
{
    if (!index.isValid()) return;
    beginRemoveRows(QModelIndex(), index.row(), index.row());
    users.removeAt(index.row());
    endRemoveRows();
}

void UsersModel::setState(int row, int state)
{
    if (row >= users.count()) return;
    users[row].state = state;
    dataChanged(createIndex(row, 0), createIndex(row, 3));
}

void UsersModel::addItem(QString name, QString MAC, QString IP)
{
    user item;
    item.name = name;
    item.MAC = MAC;
    item.wsIP = IP;
    beginInsertRows(QModelIndex(), users.count(), users.count());
    users.append(item);
    endInsertRows();
}

void UsersModel::editItem(QModelIndex index, QString name, QString MAC, QString IP)
{
    if (!index.isValid()) return;
    users[index.row()].name = name;
    users[index.row()].MAC = MAC;
    users[index.row()].wsIP = IP;
    dataChanged(createIndex(index.row(), 0), createIndex(index.row(), 3));
}


void UsersModel::listClear()
{
    if (users.count() > 0) {
        beginRemoveRows(QModelIndex(), 0, users.count() - 1);
        users.clear();
        endRemoveRows();
    }
}
