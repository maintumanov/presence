#ifndef EDITUSER_H
#define EDITUSER_H

#include <QDialog>

namespace Ui {
class EditUser;
}

class EditUser : public QDialog
{
    Q_OBJECT

public:
    explicit EditUser(QWidget *parent = Q_NULLPTR);
    ~EditUser();
    QString getName();
    QString getMAC();
    QString getIP();

    void setName(QString name);
    void setMAC(QString mac);
    void setIP(QString ip);

private slots:

    void on_buttonOk_clicked();

private:
    Ui::EditUser *ui;


};

#endif // EDITUSER_H
